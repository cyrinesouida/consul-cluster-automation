# consul-cluster-automation

This project is about the automation of the creation, management and deployment of a HA consul cluster

## Steps to implement:

### Initial setup

- Creation of an AWS account and configuration of access keys
- Installation of Terraform & Packer


### Build packer image
```
# cd packer
# packer build consul.json
```
### Configure AWS variables
```
# export AWS_REGION="eu-west-1"
# export AMI_ID="<consul ami built with packer>"
```
### Build consul cluster with terraform
 ```
# cd  /terraform/consul
terraform init
# terraform plan -var region=$AWS_REGION -var ami=$AMI_ID
# terraform apply -var region=$AWS_REGION -var ami=$AMI_ID
```
### Destroy consul cluster afterwards
```
terraform destroy -var region=$AWS_REGION -var ami=$AMI_ID
```
### Price approximation per month
- Elastic compute cloud: 3 EC2 instances for each node ; type t2 micro : 29,39 USD
- Elastic load balancing : 18,40 USD
