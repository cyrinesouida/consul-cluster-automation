variable "region" {
  description = "The region to deploy the cluster in, e.g: us-east-1."
}

variable "instance_size" {
  description = "The size of the cluster nodes, e.g: t2.micro"
}

variable "image" {
  description = "Image id, e.g: ami-0bb37375a834c7bb3"
}

variable "asg_min_size" {
  description = "The minimum size of the cluster"
}

variable "asg_max_size" {
  description = "The maximum size of the cluster"
}

variable "vpc_cidr" {
  description = "The CIDR block for the VPC"
}

variable "subnetaz1" {
  description = "The AZ for the first public subnet, e.g: eu-west-1a"
  type        = "map"
}

variable "subnetaz2" {
  description = "The AZ for the second public subnet, e.g: eu-west-1b"
  type        = "map"
}

variable "subnetaz3" {
  description = "The AZ for the third public subnet, e.g: eu-west-1c"
  type        = map
}

variable "subnet_cidr1" {
  description = "The CIDR block for the first public subnet"
}

variable "subnet_cidr2" {
  description = "The CIDR block for the second public subnet"
}

variable "subnet_cidr3" {
  description = "The CIDR block for the third public subnet"
}

variable "key_name" {
  description = "The name of the key to user for ssh access"
}

variable "public_key_path" {
  description = "The local public key path"
}

variable "asgname" {
  description = "The auto-scaling group name"
}